import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddGeneralCqrsCommand } from './add-generalcqrs.command';
import { GeneralCqrsAggregateService } from '../../aggregates/generalcqrs-aggregate/generalcqrs-aggregate.service';

@CommandHandler(AddGeneralCqrsCommand)
export class AddGeneralCqrsCommandHandler implements ICommandHandler<AddGeneralCqrsCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: GeneralCqrsAggregateService,
  ) { }
  async execute(command: AddGeneralCqrsCommand) {
    const { generalCqrsPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addGeneralCqrs(generalCqrsPayload, clientHttpRequest);
    aggregate.commit();
  }
}
