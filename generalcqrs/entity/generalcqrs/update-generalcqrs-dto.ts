import { IsNotEmpty } from 'class-validator'
export class UpdateGeneralCqrsDto {
    @IsNotEmpty()
    uuid: string
}
