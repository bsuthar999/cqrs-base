import { InjectRepository } from '@nestjs/typeorm';
import { GeneralCqrs } from './generalcqrs.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class GeneralCqrsService {
  constructor(
    @InjectRepository(GeneralCqrs)
    private readonly generalCqrsRepository: MongoRepository<GeneralCqrs>,
  ) { }

  async find(query?) {
    return await this.generalCqrsRepository.find(query);
  }

  async create(generalCqrs: GeneralCqrs) {
    const generalCqrsObject = new GeneralCqrs();
    Object.assign(generalCqrsObject, generalCqrs);
    return await this.generalCqrsRepository.insertOne(generalCqrsObject);
  }

  async findOne(param, options?) {
    return await this.generalCqrsRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.generalCqrsRepository.manager.connection
      .getMetadata(GeneralCqrs)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.generalCqrsRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.generalCqrsRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.generalCqrsRepository.deleteOne(query, options)
  }

  async updateOne(query, options?) {
    return await this.generalCqrsRepository.updateOne(query, options)
  }

}
