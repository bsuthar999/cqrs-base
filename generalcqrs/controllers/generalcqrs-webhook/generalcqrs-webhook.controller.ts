import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { GeneralCqrsWebhookDto } from '../../entity/generalcqrs/generalcqrs-webhook-dto';
import { GeneralCqrsWebhookAggregateService } from '../../aggregates/generalcqrs-webhook-aggregate/generalcqrs-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('generalCqrs')
export class GeneralCqrsWebhookController {
  constructor(
    private readonly generalCqrsWebhookAggreagte: GeneralCqrsWebhookAggregateService,
  ) { }
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  generalCqrsCreated(@Body() generalCqrsPayload: GeneralCqrsWebhookDto) {
    return this.generalCqrsWebhookAggreagte.generalCqrsCreated(generalCqrsPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  generalCqrsUpdated(@Body() generalCqrsPayload: GeneralCqrsWebhookDto) {
    return this.generalCqrsWebhookAggreagte.generalCqrsUpdated(generalCqrsPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  generalCqrsDeleted(@Body() generalCqrsPayload: GeneralCqrsWebhookDto) {
    return this.generalCqrsWebhookAggreagte.generalCqrsDeleted(generalCqrsPayload);
  }
}
