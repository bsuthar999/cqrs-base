import { Controller, Post, UseGuards, UsePipes, Body, ValidationPipe, Req, Param, Get, Query } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { GeneralCqrsDto } from '../../entity/generalcqrs/generalcqrs-dto';
import { AddGeneralCqrsCommand } from '../../command/add-generalcqrs/add-generalcqrs.command';
import { RemoveGeneralCqrsCommand } from '../../command/remove-generalcqrs/remove-generalcqrs.command';
import { UpdateGeneralCqrsCommand } from '../../command/update-generalcqrs/update-generalcqrs.command';
import { RetrieveGeneralCqrsQuery } from '../../query/get-generalcqrs/retrieve-generalcqrs.query';
import { RetrieveGeneralCqrsListQuery } from '../../query/list-generalcqrs/retrieve-generalcqrs-list.query';
import { UpdateGeneralCqrsDto } from '../../entity/generalcqrs/update-generalcqrs-dto';

@Controller('generalCqrs')
export class GeneralCqrsController {

  constructor(private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus) { }

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() generalCqrsPayload: GeneralCqrsDto, @Req() req) {
    return this.commandBus.execute(new AddGeneralCqrsCommand(generalCqrsPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveGeneralCqrsCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveGeneralCqrsQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveGeneralCqrsListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateGeneralCqrsDto) {
    return this.commandBus.execute(new UpdateGeneralCqrsCommand(updatePayload));
  }
}
