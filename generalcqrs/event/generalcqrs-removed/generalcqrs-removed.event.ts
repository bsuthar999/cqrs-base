import { IEvent } from '@nestjs/cqrs';
import { GeneralCqrs } from '../../entity/generalcqrs/generalcqrs.entity';

export class GeneralCqrsRemovedEvent implements IEvent {
  constructor(public generalCqrs: GeneralCqrs) { }
}
