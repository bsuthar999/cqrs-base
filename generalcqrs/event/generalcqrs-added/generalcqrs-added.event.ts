import { IEvent } from '@nestjs/cqrs';
import { GeneralCqrs } from '../../entity/generalcqrs/generalcqrs.entity';

export class GeneralCqrsAddedEvent implements IEvent {
  constructor(public generalCqrs: GeneralCqrs, public clientHttpRequest: any) { }
}
