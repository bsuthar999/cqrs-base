import { IEvent } from '@nestjs/cqrs';
import { GeneralCqrs } from '../../entity/generalcqrs/generalcqrs.entity';

export class GeneralCqrsUpdatedEvent implements IEvent {
  constructor(public updatePayload: GeneralCqrs) { }
}
