import { GeneralCqrsAggregateService } from "./generalcqrs-aggregate/generalcqrs-aggregate.service";
import { GeneralCqrsWebhookAggregateService } from "./generalcqrs-webhook-aggregate/generalcqrs-webhook-aggregate.service";

export const GeneralCqrsAggregatesManager = [GeneralCqrsAggregateService, GeneralCqrsWebhookAggregateService];
