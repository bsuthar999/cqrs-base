import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { GeneralCqrsDto } from '../../entity/generalcqrs/generalcqrs-dto';
import { GeneralCqrs } from '../../entity/generalcqrs/generalcqrs.entity';
import { GeneralCqrsAddedEvent } from '../../event/generalcqrs-added/generalcqrs-added.event';
import { GeneralCqrsService } from '../../entity/generalcqrs/generalcqrs.service';
import { GeneralCqrsRemovedEvent } from '../../event/generalcqrs-removed/generalcqrs-removed.event';
import { GeneralCqrsUpdatedEvent } from '../../event/generalcqrs-updated/generalcqrs-updated.event';
import { UpdateGeneralCqrsDto } from '../../entity/generalcqrs/update-generalcqrs-dto';

@Injectable()
export class GeneralCqrsAggregateService extends AggregateRoot {
  constructor(
    private readonly generalCqrsService: GeneralCqrsService
  ) {
    super();
  }

  addGeneralCqrs(generalCqrsPayload: GeneralCqrsDto, clientHttpRequest) {
    const generalCqrs = new GeneralCqrs();
    Object.assign(generalCqrs, generalCqrsPayload)
    generalCqrs.uuid = uuidv4();
    this.apply(new GeneralCqrsAddedEvent(generalCqrs, clientHttpRequest));
  }

  async retrieveGeneralCqrs(uuid: string, req) {
    const provider = await this.generalCqrsService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getGeneralCqrsList(offset, limit, sort, search, clientHttpRequest) {
    return await this.generalCqrsService.list(offset, limit, search, sort);
  }


  async remove(uuid: string) {
    const found = await this.generalCqrsService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new GeneralCqrsRemovedEvent(found));
  }

  async update(updatePayload: UpdateGeneralCqrsDto) {
    const provider = await this.generalCqrsService.findOne({ uuid: updatePayload.uuid });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new GeneralCqrsUpdatedEvent(update));
  }
}
