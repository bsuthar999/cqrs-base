import { Test, TestingModule } from '@nestjs/testing';
import { GeneralCqrsAggregateService } from './generalcqrs-aggregate.service';
import { GeneralCqrsService } from '../../entity/generalcqrs/generalcqrs.service';


describe('GeneralCqrsAggregateService', () => {
  let service: GeneralCqrsAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeneralCqrsAggregateService,
        {
          provide: GeneralCqrsService,
          useValue: {},
        }],
    }).compile();

    service = module.get<GeneralCqrsAggregateService>(GeneralCqrsAggregateService);
  });
  GeneralCqrsAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
