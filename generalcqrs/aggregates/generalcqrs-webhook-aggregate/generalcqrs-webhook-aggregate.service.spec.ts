import { Test, TestingModule } from '@nestjs/testing';
import { GeneralCqrsWebhookAggregateService } from './generalcqrs-webhook-aggregate.service';
import { GeneralCqrsService } from '../../../generalcqrs/entity/generalcqrs/generalcqrs.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';

describe('GeneralCqrsWebhookAggregateService', () => {
  let service: GeneralCqrsWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GeneralCqrsWebhookAggregateService,
        {
          provide: GeneralCqrsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<GeneralCqrsWebhookAggregateService>(
      GeneralCqrsWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
